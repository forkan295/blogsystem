<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeControllerr@index')->name("home");
Route::get('/single-view/{slug}','HomeControllerr@singlePost')->name("post.single");
Route::get('/all-post','HomeControllerr@allPost')->name("post.all");

Route::get('/catrgory/{slug}','HomeControllerr@categoryPost')->name("category.post");

Route::get('/tag/{slug}','HomeControllerr@tagPost')->name("tag.post");

route::post('subcriber','SubscriberController@store')->name('subscriber.store');

route::get('search','SearchController@search')->name('search');
Auth::routes();

Route::group(['middleware'=>['auth']],function()
{
   route::post('favorite/{id}/add','FavoriteController@add')->name('post.favorite');
   route::post('comment/{id}','CommerntController@store')->name('post.comment');

});
route::get('/comment','CommerntController@view')->name('view.comment');
Route::group(['as'=>'admin.','prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth','admin']],function(){

     route::get("/dashboard","DashboardController@index")->name("dashboard");

     route::get('settings','SettingsController@index')->name('settings');
     route::put('profile-update','SettingsController@profileUpdate')->name('profile.update');
     route::put('password-update','SettingsController@passwordUpdate')->name('password.update');

     route::resource('tag','TagController');

     route::resource('category','CategoryController');

     route::get('favorite','FavoriteController@show')->name('favorite.show');
     
     route::get('post/pending','PostController@pending')->name('post.pending');
     route::put('post/{id}/approved','PostController@approval')->name('post.approved');
     route::resource('post','PostController');
     
     route::get('subscriber','SubscriberController@index')->name('subscriber.index');
     route::delete('subscriber/{id}','SubscriberController@destroy')->name('subscriber.destroy');

     route::get('comments','CommentController@index')->name('comment.index');
     route::delete('comments/{id}','CommentController@destroy')->name('comment.destroy');
     
    

    
});
Route::group(['as'=>'author.','prefix'=>'author','namespace'=>'Author','middleware'=>['auth','author']], function(){

    route::get("/dashboard","DashboardController@index")->name("dashboard");
    route::resource('post','PostController');

    route::get('favorite','FavoriteController@show')->name('favorite.show');

    route::get('settings','SettingsController@index')->name('settings');
    route::put('profile-update','SettingsController@profileUpdate')->name('profile.update');
    route::put('password-update','SettingsController@passwordUpdate')->name('password.update');

    route::get('comments','CommentController@index')->name('comment.index');
     route::delete('comments/{id}','CommentController@destroy')->name('comment.destroy');

});
