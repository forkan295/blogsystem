<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscriber;
use Brian2694\Toastr\Facades\Toastr;

class SubscriberController extends Controller
{
    public function store(Request $request)
    {

        $request->validate([
            'email'=>'required|unique:subscribers|email'
        ]);
          $subs = new Subscriber();

          $subs->email = $request->email;
          $subs->save();

          Toastr::success('Your successfully subscribe in our website','success');
          return redirect()->back();

    }

  
}
