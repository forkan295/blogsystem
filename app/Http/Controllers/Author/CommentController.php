<?php

namespace App\Http\Controllers\Author;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function index()
    {
 
        $data['comments'] = Auth::user()->comments;
        
        return view('author.comment.index',$data);
        
    }
    public function destroy($id)
    {
 
        $comment = Comment::find($id);
       $comment->delete();
       Toastr::error('comment successfully deleted','Delete');
       return redirect()->back();
        
    }
}
