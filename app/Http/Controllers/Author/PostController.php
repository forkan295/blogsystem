<?php

namespace App\Http\Controllers\Author;

use App\Tag;
use App\Post;
use App\User;
use App\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewAuthor;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts["data"] = Auth::User()->posts()->latest()->get();
        return view('author.post.index',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories['catData'] = Category::all();
        $tags['tagData'] = Tag::all();
        return view('author.post.create',$categories,$tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd(Auth::User()->name);
        $request->validate([
            'title'=>'required',
            'image'=>'required|image|max:2048',
            'categories'=>'required',
            'tags'=>'required',
            'body'=>'required',
           ]);
    
           
    
        //    dd($image);
           $slug = str_slug($request->title).'-'.uniqid();
           $slugs = str_slug($request->title);
    
          if($request->hasFile('image')){
    
              $image = $request->file('image');
              $currentDate = Carbon::now()->toDateString();
              $imageName = $slugs."-".$currentDate."-".uniqid().Auth::User()->name.".".$image->getClientOriginalExtension(); 
    
              $path = "storage/post/".$imageName;
    
              if(!Storage::disk('public')->exists('post'))
              {
                     Storage::disk('public')->makeDirectory('post');
              }
              $newImage = Image::make($image)->resize(1600,1055)->save($path);
    
            //   Storage::disk('public')->put('post/'.$imageName , $newImage );
    
         }
         else
         {
              $imageName = "default.png";
          }
    
          $post = new Post();
          $post->user_id = Auth::id();
          $post->title = $request->title;
          $post->image = $imageName;
          $post->slug = $slug;
    
          if(isset($request->status))
          {
            $post->status = true;
          }else{
    
            $post->status = false;
          }
          $post->body = $request->body;
          $post->is_approved = false;
    
          $post->save();
    
          $post->categories()->attach($request->categories);
          $post->tags()->attach($request->tags);
    
           $user = User::where('role_id','1')->get();
           Notification::send($user,new NewAuthor($post));

          Toastr::success("success fully added posts","success");
          return redirect()->route("author.post.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {

      if($post->user_id != Auth::id())
       {
         Toastr::error('You are no authorized to access this post','error');
         return redirect()->back();
       }
      $categories = Category::all();
      $tags = Tag::all();
      return view('author.post.show',compact('post','categories','tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {

       if($post->user_id != Auth::id())
       {
         Toastr::error('You are no authorized to access this post','error');
         return redirect()->back();
       }
        $categories = Category::all();
        $tags = Tag::all();
        return view('author.post.edit',compact('post','categories','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {

      if($post->user_id != Auth::id())
       {
         Toastr::error('You are no authorized to access this post','error');
         return redirect()->back();
       }
        $request->validate([
            'title'=>'required',
            'image'=>'image|max:2048',
            'categories'=>'required',
            'tags'=>'required',
            'body'=>'required',
           ]);
    
           
    
        //    dd($image);
           $slug = str_slug($request->title).'-'.uniqid();
           $slugs = str_slug($request->title);
    
          if($request->hasFile('image')){
    
              $image = $request->file('image');
              $currentDate = Carbon::now()->toDateString();
              $imageName = $slugs."-".$currentDate."-".uniqid().".".$image->getClientOriginalExtension(); 
    
              $path = "storage/post/".$imageName;
    
              if(!Storage::disk('public')->exists('post'))
              {
                     Storage::disk('public')->makeDirectory('post');
              }

              if(Storage::disk('public')->exists('post/'.$post->image))
              {
                Storage::disk('public')->delete('post/'.$post->image);
              }
              $newImage = Image::make($image)->resize(1600,1055)->save($path);
    
            //   Storage::disk('public')->put('post/'.$imageName , $newImage );
    
         }
        else
         {
            $imageName = $post->image;
         }
    
          
          $post->user_id = Auth::id();
          $post->title = $request->title;
          $post->image = $imageName;
          $post->slug = $slug;
    
          if(isset($request->status))
          {
            $post->status = true;
          }else{
    
            $post->status = false;
          }
          $post->body = $request->body;
          $post->is_approved = false;
    
          $post->save();
    
          $post->categories()->sync($request->categories);
          $post->tags()->sync($request->tags);
    
          Toastr::success("success fully Updated posts","success");
          return redirect()->route("author.post.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
      if($post->user_id != Auth::id())
       {
         Toastr::error('You are no authorized to access this post','error');
         return redirect()->back();
       }
       
      if(Storage::disk('public')->exists("post/".$post->image))
      {
        Storage::disk('public')->delete('post/'.$post->image);
      }


      $post->delete();
      $post->categories()->detach();
      $post->tags()->detach();
      Toastr::success("Post successfully deleted","success");
      return redirect()->back();
    }
}
