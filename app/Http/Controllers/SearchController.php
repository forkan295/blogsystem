<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $query = $request->input('query');
        $data['posts'] = Post::where('title','LIKE',"%$query%")->adminApprovalPost()->published()->get();
        $data['query'] = $request->input('query');
        return view('layouts.frontend.search',$data);
    }
}
