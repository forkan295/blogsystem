<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;


class CommentController extends Controller
{
   public function index()
   {

       $data['comments'] = Comment::all();
       return view('admin.comment.index',$data);
       
   }
   public function destroy($id)
   {

       $comment = Comment::find($id);
      $comment->delete();
      Toastr::error('comment successfully deleted','Delete');
      return redirect()->back();
       
   }
}
