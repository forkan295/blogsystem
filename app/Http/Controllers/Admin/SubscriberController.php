<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscriber;
use Brian2694\Toastr\Facades\Toastr;

class SubscriberController extends Controller
{
    public function index()
    {
       $subs['data'] = Subscriber::latest()->get();
       return view('admin.subscriber.index',$subs);
    }

    public function destroy($id)
    {
        $subs = Subscriber::find($id);
        $subs->delete();
        Toastr::success('successfully deleted subscriber','success');
        return redirect()->back();
        
    }
}
