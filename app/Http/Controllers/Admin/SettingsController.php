<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
  public function index()
  {
      return view('admin.settings');
  }

  public function profileUpdate(Request $request)
  {
     $request->validate([
       'name'=>'required',
       'email'=>'required|email',
       'image'=>'required|image',

     ]);


     $image = $request->file('image');
     $slug = str_slug($request->name);
     $user = User::findOrFail(Auth::id());
    
     if(isset($image))
     {
       $currentDate = Carbon::now()->toDateString();
       $imageName = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

       $path = "storage/profile/".$imageName;
      
       if(!Storage::disk('public')->exists('profile'))
       {
         Storage::disk('public')->makeDirectory('profile');

       }

       if(Storage::disk('public')->exists('profile/'.$user->image))
        {
          Storage::disk('public')->delete('profile/'.$user->image);
        }

       $profile = Image::make($image)->resize(500,500)->save($path);

      //  Storage::disk('public')->put('profile/'.$imageName,$profile);


     }else{
       $imageName = $user->image;
     }
     $user->name = $request->name;
     $user->email = $request->email;
     $user->image = $imageName;
     $user->about = $request->about;
     $user->save();
     Toastr::success('your profile has been updated successfully','success');
     return redirect()->back();
  }

  public function passwordUpdate(Request $request)
    {
      $request->validate([
           'old_password' => 'required',
           'password' => 'required|confirmed',

      ]);
     
      $currentPass = Auth::user()->password;
      if(Hash::check($request->old_password,$currentPass))
      {
        if(!Hash::check($request->password,$currentPass))  
        {
          
          $user = User::find(Auth::id());
          
          $user->password = Hash::make($request->password);
          $user->save();
          Toastr::success('password has been changed successfully','success');
          Auth::logout();
          return redirect()->back();
        } else {
          {
            Toastr::error('Your old password should not match with new password','error');
            return redirect()->back();
          }
        }

      }else{
        Toastr::error('current password does not match with old password','error');
        return redirect()->back();
      }
    }
}
