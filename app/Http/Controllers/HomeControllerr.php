<?php

namespace App\Http\Controllers;



use App\Tag;
use App\Post;
use App\Comment;
use App\Category;
use Illuminate\Http\Request;


class HomeControllerr extends Controller
{
   public function index()
   {

     $data['posts'] = Post::latest()->adminApprovalPost()->published()->take('10')->get();
     return view('welcome',$data);
   }


   public function singlePost($slug)
   {  

    $data['post'] = Post::where('slug',$slug)->adminApprovalPost()->published()->first();

    

    $blogPost = "blog_".$data['post']->id;

        if(!session()->has($blogPost))
        {
            $data['post']->increment('view_count');
            session()->put($blogPost,1);
        }

      $data['randomPost'] = Post::adminApprovalPost()->published()->take(3)->inRandomOrder()->get(); 
      
     return view('layouts.frontend.singlepost',$data);
     }
     
     public function allPost()
      {
        $data['allPost'] = Post::latest()->adminApprovalPost()->published()->paginate(9);
        return view('layouts.frontend.allpost',$data);
      }
     public function categoryPost($slug)
      {
        $data['CategoryPost'] = Category::where('slug',$slug)->first(); 
        $data['posts'] = $data['CategoryPost']->posts()->adminApprovalPost()->published()->get();
        return view('layouts.frontend.catpost',$data);
      }
     public function tagPost($slug)
      {
        $data['Tag'] = Tag::where('slug',$slug)->first();
        $data['tagposts'] = $data['Tag']->posts()->adminApprovalPost()->published()->get();
        return view('layouts.frontend.tagpost',$data);
      }
}
