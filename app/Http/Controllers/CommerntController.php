<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;


class CommerntController extends Controller
{
    public function store(Request $request,$id)
    {
        
          $request->validate([
             'comment'=>'required'
          ]);

          $comment = new Comment();
          $comment->comment = $request->comment;
          $comment->post_id = $id;
          $comment->user_id = Auth::user()->id;
          $comment->save();
          Toastr::success('comment has been successvully added','success');
          return redirect()->back();
    }

  

   
}
