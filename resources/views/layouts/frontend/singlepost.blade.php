@extends('layouts.frontend.app')
@section('tittle','login')


@push('css')
	<link href="{{ asset('assets/frontend/css/home/styles.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/frontend/css/post-1/styles.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/frontend/css/home/responsive.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/frontend/css/post-1/responsive.css') }}" rel="stylesheet">
	
	<style>
		.fav-color 
		{
			color: blueviolet;
		}
		.post-slider
		{
			height: 500px;
			background-position: center;
			background-size: cover;
			background-image: url({{ url('storage/post/'.$post->image) }});
			
		}
		.txt
		{
			background-color:#6bcbdc;
			opacity:0.5;
			padding: 10px;
			border: 1px solid #6bcbdc;
			border-radius: 20px; 
		}

	</style>
    
@endpush

@section('contant')


<div class="post-slider">
		<div class="display-table  center-text">
			<h1 class="title display-table-cell text-white "><b class="txt">{{ $post->title }}</b></h1>
		</div>
	</div><!-- slider -->

	<section class="post-area section">
		<div class="container">

			<div class="row">

				<div class="col-lg-8 col-md-12 no-right-padding">

					<div class="main-post">

						<div class="blog-post-inner">

							<div class="post-info">

								<div class="left-area">
									<a class="avatar" href="#"><img src="{{ url('storage/profile/'.$post->user->image) }}" alt="Profile Image"></a>
								</div>

								<div class="middle-area">
									<a class="name" href="#"><b>{{ $post->user->name}}</b></a>
									<h6 class="date">on {{ ($post->created_at) ? $post->created_at->toFormattedDateString() : 'No create date availabe' }}</h6>
								</div>

							</div><!-- post-info -->

							<h3 class="title"><a href="#"><b>{{ $post->title }}</b></a></h3>

							<p class="para">
                                {!! $post->body !!}
                            </p>

							<ul class="tags">
                                @foreach ($post->tags as $tag)
                                <li><a href="{{ route('tag.post',$tag->slug) }}">{{ $tag->name }}</a></li>
                                @endforeach
							</ul>
						</div><!-- blog-post-inner -->

						<div class="post-icons-area">
							<ul class="post-icons">
								<li><a href="#"><i class="ion-heart"></i>{{ $post->favorite_to_users->count() }}</a></li>
								<li><a href="#"><i class="ion-chatbubble"></i>6</a></li>
                            <li><a href="#"><i class="ion-eye"></i>{{ $post->view_count }}</a></li>
							</ul>

							<ul class="icons">
								<li>SHARE : </li>
								<li><a href="#"><i class="ion-social-facebook"></i></a></li>
								<li><a href="#"><i class="ion-social-twitter"></i></a></li>
								<li><a href="#"><i class="ion-social-pinterest"></i></a></li>
							</ul>
						</div>

						<div class="post-footer post-info">

							<div class="left-area">
								<a class="avatar" href="#"><img src="{{ url('storage/profile/'.$post->user->image) }}" alt="Profile Image"></a>
							</div>

							<div class="middle-area">
								<a class="name" href="#"><b>{{ $post->user->name}}</b></a>
								<h6 class="date">on {{ ($post->created_at) ? $post->created_at->toFormattedDateString() : 'No create date availabe' }}</h6>
							</div>

						</div><!-- post-info -->


					</div><!-- main-post -->
				</div><!-- col-lg-8 col-md-12 -->

				<div class="col-lg-4 col-md-12 no-left-padding">

					<div class="single-post info-area">

						<div class="sidebar-area about-area">
							<h4 class="title"><b>ABOUT BONA</b></h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
								ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur
								Ut enim ad minim veniam</p>
						</div>

						<div class="sidebar-area subscribe-area">

							<h4 class="title"><b>SUBSCRIBE</b></h4>
							<div class="input-area">
								<form>
									<input class="email-input" type="text" placeholder="Enter your email">
									<button class="submit-btn" type="submit"><i class="icon ion-ios-email-outline"></i></button>
								</form>
							</div>

						</div><!-- subscribe-area -->

						<div class="tag-area">

							<h4 class="title"><b>Categories</b></h4>
							<ul>
								
								@foreach ($post->categories as $category)
                                <li><a href="{{ route('category.post',$category->slug) }}">{{ $category->name }}</a></li>
                                @endforeach
							</ul>

						</div><!-- subscribe-area -->

					</div><!-- info-area -->

				</div><!-- col-lg-4 col-md-12 -->

			</div><!-- row -->

		</div><!-- container -->
	</section><!-- post-area -->


	<section class="recomended-area section">
		<div class="container">
			<div class="row">

                @foreach ($randomPost as $randompost)
                    
                <div class="col-lg-4 col-md-6">
                        <div class="card h-100">
                                <div class="single-post post-style-1">
            
                                    <div class="blog-image"><img src="{{ url('storage/post/'. $randompost->image)}}" alt="Blog Image"></div>
            
                                    <a class="avatar" href="{{ route('post.single',$randompost->slug) }}"><img src="{{ url('storage/profile/'.$randompost->user->image)}}" alt="Profile Image"></a>
            
                                    <div class="blog-info">
            
                                        <h4 class="title"><a href="#"><b>{{ $randompost->title }}</b></a></h4>
            
                                        <ul class="post-footer">      
                                            <li>
                                                @guest
                                                    <a href="#" onclick ="toastr.info('to add favorite list you have to login first','info',{closeButton:true,progressBar:true})">
                                                    <i class="ion-heart"></i>{{ $randompost->favorite_to_users->count()}}</a>
                                                @else
                                                    <a href="#" onclick ="document.getElementById('favorite-form-{{ $randompost->id }}').submit();"
                                                        class="{{ 
                                                                  Auth::user()->favorite_posts->where('pivot.post_id',$randompost->id)->count() != 0 ? 'fav-color' : '' 
                                                               }}">
                                                    <i class="ion-heart"></i>{{ $randompost->favorite_to_users->count() }}</a>
            
                                                    <form id="favorite-form-{{ $randompost->id }}" action="{{ route('post.favorite',$randompost->id) }}" style="display:none;" method="POST">
                                                        @csrf
                                                    </form>
                                                @endguest
                                                
                                               
                                            
                                            </li>
                                            <li><a href="#"><i class="ion-chatbubble"></i>6</a></li>
                                            <li><a href="#"><i class="ion-eye"></i>{{ $randompost->view_count }}</a></li>
                                        </ul>
            
                                    </div><!-- blog-info -->
                                </div><!-- single-post -->
                            </div><!-- card -->
				</div><!-- col-md-6 col-sm-12 -->

               

                @endforeach

				

			

			</div><!-- row -->

		</div><!-- container -->
	</section>

	<section class="comment-section">
		<div class="container">
			<h4><b>POST COMMENT</b></h4>
			<div class="row">

				<div class="col-lg-8 col-md-12">
					@guest

					@else
					<div class="comment-form">
							<form method="post" action="{{ route('post.comment',$post->id) }}">
								@csrf
								<div class="row">	
	
									<div class="col-sm-12">
										<textarea name="comment" rows="2" class="text-area-messge form-control"
											placeholder="Enter your comment" aria-required="true" aria-invalid="false"></textarea >
									</div><!-- col-sm-12 -->
									<div class="col-sm-12">
										<button class="submit-btn" type="submit" id="form-submit"><b>POST COMMENT</b></button>
									</div><!-- col-sm-12 -->
	
								</div><!-- row -->
							</form>
						</div><!-- comment-form -->
					@endguest
					<h4><b> Comments ({{ $post->comments()->count()}})</b></h4>

					@php
						$i = 1;
					@endphp

				   @foreach ($post->comments as $comment) 

				   @if ($i<= 5)
				   <div class="commnets-area ">

						<div class="comment">

							<div class="post-info">

								<div class="left-area">
									<a class="avatar" href="#"><img src="{{ url('storage/profile/'.$comment->user->image) }}" alt="Profile Image"></a>
								</div>

								<div class="middle-area">
									<a class="name" href="#"><b>{{ $comment->user->name }}</b></a>
									<h6 class="date">on {{ $comment->created_at->toFormattedDateString() }}</h6>
								</div>

								<div class="right-area">
									<h5 class="reply-btn" ><a href="#"><b>REPLY</b></a></h5>
								</div>

							</div><!-- post-info -->

							<p>{{ $comment->comment }}</p>

						</div>
					</div><!-- commnets-area -->
						@php
							$i++;
						@endphp
				   @endif

					
				   
				   
				   @endforeach
				  
				   
				  
					

					<a class="more-comment-btn" href="#"><b>VIEW MORE COMMENTS</a>

				</div><!-- col-lg-8 col-md-12 -->

			</div><!-- row -->

		</div><!-- container -->
	</section> 
@endsection
    
@push('js')
<script src="{{ asset('assets/frontend/js/swiper.js') }}"></script>
@endpush
