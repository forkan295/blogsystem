@extends('layouts.frontend.app')
@section('tittle','login')


@push('css')
	<link href="{{ asset('assets/frontend/css/home/styles.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/frontend/css/post-1/styles.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/frontend/css/home/responsive.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/frontend/css/post-1/responsive.css') }}" rel="stylesheet">
	
	<style>
		.fav-color 
		{
			color: blueviolet;
		}
		.post-slider
		{
			height: 500px;
			background-position: center;
			background-size: cover;
			background-image: url({{ url('storage/post/cover.jpg') }});
			
		}

	</style>
    
@endpush

@section('contant')
<div class="post-slider">
		<div class="display-table  center-text">
			<h1 class="title display-table-cell text-white"><b>All Posts</b></h1>
		</div>
	</div><!-- slider -->
	<section class="recomended-area section">
		<div class="container">
			<div class="row">

                @foreach ($allPost as $post)
                <div class="col-lg-4 col-md-6">
                        <div class="card h-100">
                                <div class="single-post post-style-1">
            
                                    <div class="blog-image"><img src="{{ url('storage/post/'. $post->image)}}" alt="Blog Image"></div>
            
                                    <a class="avatar" href="{{ route('post.single',$post->slug) }}"><img src="{{ url('storage/profile/'.$post->user->image)}}" alt="Profile Image"></a>
            
                                    <div class="blog-info">
            
                                        <h4 class="title"><a href="{{ route('post.single',$post->slug) }}"><b>{{ $post->title }}</b></a></h4>
            
                                        <ul class="post-footer">      
                                            <li>
                                                @guest
                                                    <a href="#" onclick ="toastr.info('to add favorite list you have to login first','info',{closeButton:true,progressBar:true})">
                                                    <i class="ion-heart"></i>{{ $post->favorite_to_users->count()}}</a>
                                                @else
                                                    <a href="#" onclick ="document.getElementById('favorite-form-{{ $post->id }}').submit();"
                                                        class="{{ 
                                                                  Auth::user()->favorite_posts->where('pivot.post_id',$post->id)->count() != 0 ? 'fav-color' : '' 
                                                               }}">
                                                    <i class="ion-heart"></i>{{ $post->favorite_to_users->count() }}</a>
            
                                                    <form id="favorite-form-{{ $post->id }}" action="{{ route('post.favorite',$post->id) }}" style="display:none;" method="POST">
                                                        @csrf
                                                    </form>
												@endguest
											</li>
                                            <li><a href="#"><i class="ion-chatbubble"></i>{{ $post->comments()->count() }}</a></li>
                                            <li><a href="#"><i class="ion-eye"></i>{{ $post->view_count }}</a></li>
                                        </ul>
                                    </div><!-- blog-info -->
                                </div><!-- single-post -->
                            </div><!-- card -->
				</div><!-- col-md-6 col-sm-12 -->

               

				@endforeach
			</div><!-- row -->
			{{ $allPost->links() }}

		</div><!-- container -->
	</section>


@endsection
    
@push('js')
<script src="{{ asset('assets/frontend/js/swiper.js') }}"></script>
@endpush
