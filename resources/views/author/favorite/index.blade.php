@extends('layouts.backend.app')

@section('tittle','Post')

@push('css')
<link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css"') }}" rel="stylesheet">
@endpush
@section('contant')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">

            <a href="{{ route('admin.post.create') }}" class="btn btn-success waves-effect">Add New Post</a>
        </div>
       
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           Subscribers
                           <span><small>{{ $favoritePost->count() }}</small></span>
                        </h2>
                       
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>

                                  
                                       
                                       <tr>
                                            <th>ID</th>
                                            <th>Title</th>
                                            <th>favorite</th>
                                            <th>view</th>
                                            <th>slug</th>
                                            <th>Image</th>
                                            <th>action</th> 
                                           
                                        </tr>
                                       
                                

                                </thead>
                                <tfoot>

                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>favorite</th>
                                        <th>view</th>
                                        <th>slug</th>
                                        <th>Image</th>     
                                        <th>action</th>     
                                    </tr>

                                </tfoot>
                                <tbody>
                                       
                                   @foreach ($favoritePost as $key => $item)
                                   
                                    <tr>
                                       
                                        <td>{{ $key + 1 }}</td>
                                      
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->favorite_to_users->count() }}</td>
                                        <td>{{ $item->view_count }}</td>
                                        <td>{{ $item->slug}}</td>
                                        <td><img src="{{ url('storage/post/'. $item->image) }}" alt="" class="thumbnail" style="height:100px; width:150px;"></td>
                                     
                                        <td>
                                            <a href="{{ route('author.post.show',$item->id) }}" class="btn btn-primary">Show</a>
                                            <button type="button"  class="btn btn-danger" onclick="deletePost({{ $item->id }})">Delete</button>
                                            <form id="delete-form-{{ $item->id }}" action="{{ route('post.favorite',$item->id) }}" method="POST" style="display:none">
                                            @csrf
                                            
                                            </form>
                                        </td>
                                    </tr>
                                   @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
@endsection

@push('js')
<script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

<script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script type="text/javascript">
function deletePost(id)
{
    Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
    event.preventDefault();
    document.getElementById('delete-form-'+ id).submit();
  }
})

}

</script>
@endpush