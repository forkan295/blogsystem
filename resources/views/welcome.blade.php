@extends('layouts.frontend.app')
@section('tittle','login')


@push('css')
	<link href="{{ asset('assets/frontend/css/home/styles.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/frontend/css/home/responsive.css') }}" rel="stylesheet">
    
    <style>
    .fav-color 
    {
        color: blueviolet;
    }
    </style>
@endpush

@section('contant')
<div class="main-slider">
    <div class="swiper-container position-static" data-slide-effect="slide" data-autoheight="false"
        data-swiper-speed="500" data-swiper-autoplay="10000" data-swiper-margin="0" data-swiper-slides-per-view="4"
        data-swiper-breakpoints="true" data-swiper-loop="true" >
        <div class="swiper-wrapper">

            @foreach ($cats as $cat)
            <div class="swiper-slide">
                <a class="slider-category" href="{{ route('category.post',$cat->slug) }}">
                    <div class="blog-image"><img src="{{ asset('storage/category/slider/'. $cat->image) }}" alt="Blog Image"></div>

                    <div class="category">
                        <div class="display-table center-text">
                            <div class="display-table-cell">
                                <h3><b>{{ $cat->name }}</b></h3>
                            </div>
                        </div>
                    </div>

                </a>
            </div><!-- swiper-slide -->
            @endforeach

            {{-- <div class="swiper-slide">
                <a class="slider-category" href="#">
                    <div class="blog-image"><img src="images/category-2-400x250.jpg" alt="Blog Image"></div>

                    <div class="category">
                        <div class="display-table center-text">
                            <div class="display-table-cell">
                                <h3><b>SPORT</b></h3>
                            </div>
                        </div>
                    </div>

                </a>
            </div><!-- swiper-slide -->

            <div class="swiper-slide">
                <a class="slider-category" href="#">
                    <div class="blog-image"><img src="images/category-3-400x250.jpg" alt="Blog Image"></div>

                    <div class="category">
                        <div class="display-table center-text">
                            <div class="display-table-cell">
                                <h3><b>HEALTH</b></h3>
                            </div>
                        </div>
                    </div>

                </a>
            </div><!-- swiper-slide -->

            <div class="swiper-slide">
                <a class="slider-category" href="#">
                    <div class="blog-image"><img src="images/category-4-400x250.jpg" alt="Blog Image"></div>

                    <div class="category">
                        <div class="display-table center-text">
                            <div class="display-table-cell">
                                <h3><b>DESIGN</b></h3>
                            </div>
                        </div>
                    </div>

                </a>
            </div><!-- swiper-slide -->

            <div class="swiper-slide">
                <a class="slider-category" href="#">
                    <div class="blog-image"><img src="images/category-5-400x250.jpg" alt="Blog Image"></div>

                    <div class="category">
                        <div class="display-table center-text">
                            <div class="display-table-cell">
                                <h3><b>MUSIC</b></h3>
                            </div>
                        </div>
                    </div>

                </a>
            </div><!-- swiper-slide -->

            <div class="swiper-slide">
                <a class="slider-category" href="#">
                    <div class="blog-image"><img src="images/category-6-400x250.jpg" alt="Blog Image"></div>

                    <div class="category">
                        <div class="display-table center-text">
                            <div class="display-table-cell">
                                <h3><b>MOVIE</b></h3>
                            </div>
                        </div>
                    </div>

                </a>
            </div><!-- swiper-slide --> --}}

        </div><!-- swiper-wrapper -->

    </div><!-- swiper-container -->

</div><!-- slider -->

<section class="blog-area section">
    <div class="container">

        <div class="row">
           
            @foreach ($posts as $post)
            {{-- {{ dd($post->user->id) }} --}}
            <div class="col-lg-4 col-md-6">
                <div class="card h-100">
                    <div class="single-post post-style-1">

                        <div class="blog-image"><img src="{{ url('storage/post/'. $post->image)}}" alt="Blog Image"></div>

                        <a class="avatar" href="{{ route('post.single',$post->slug) }}"><img src="{{ url('storage/profile/'.$post->user->image)}}" alt="Profile Image"></a>

                        <div class="blog-info">

                            <h4 class="title"><a href="{{ route('post.single',$post->slug) }}"><b>{{ $post->title }}</b></a></h4>

                            <ul class="post-footer">      
                                <li>
                                    @guest
                                        <a href="#" onclick ="toastr.info('to add favorite list you have to login first','info',{closeButton:true,progressBar:true})">
                                        <i class="ion-heart"></i>{{ $post->favorite_to_users->count()}}</a>
                                    @else
                                        <a href="#" onclick ="document.getElementById('favorite-form-{{ $post->id }}').submit();"
                                            class="{{ 
                                                      Auth::user()->favorite_posts->where('pivot.post_id',$post->id)->count() != 0 ? 'fav-color' : '' 
                                                   }}">
                                        <i class="ion-heart"></i>{{ $post->favorite_to_users->count() }}</a>

                                        <form id="favorite-form-{{ $post->id }}" action="{{ route('post.favorite',$post->id) }}" style="display:none;" method="POST">
                                            @csrf
                                        </form>
                                    @endguest
                                    
                                   
                                
                                </li>
                                <li><a href="#"><i class="ion-chatbubble"></i>{{ $post->comments->count() }}</a></li>
                                <li><a href="#"><i class="ion-eye"></i>{{ $post->view_count }}</a></li>
                            </ul>

                        </div><!-- blog-info -->
                    </div><!-- single-post -->
                </div><!-- card -->
            </div><!-- col-lg-4 col-md-6 -->
            @endforeach

            
        </div><!-- row -->

        <a class="load-more-btn" href="#"><b>LOAD MORE</b></a>

    </div><!-- container -->
</section><!-- section -->
@endsection
    
@push('js')
<script src="{{ asset('assets/frontend/js/swiper.js') }}"></script>
@endpush
