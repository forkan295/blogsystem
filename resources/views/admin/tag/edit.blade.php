@extends('layouts.backend.app')

@section('tittle','tag')

@push('css')

@endpush
@section('contant')
<section class="content">
        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                              Add new tag    
                            </h2>
                        
                        </div>
                        <div class="body">
                            <form action="{{ route('admin.tag.update',$tag->id )}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" id="tagname" class="form-control" name="name" value="{{ $tag->name }}">
                                        <label class="form-label">Tag name</label>
                                    </div>
                                </div>
                                <br>
                                <a href="{{ route('admin.tag.index') }}" class="btn btn-danger m-t-15 waves-effect">Back</a>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
</section>
@endsection

@push('js')

@endpush