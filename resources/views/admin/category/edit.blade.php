@extends('layouts.backend.app')

@section('tittle','category')

@push('css')

@endpush
@section('contant')
<section class="content">
        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                              Update new category    
                            </h2>
                        
                        </div>
                        <div class="body">
                            <form action="{{ route('admin.category.update',$category->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" id="tagname" class="form-control" name="name" value="{{ $category->name }}">
                                        <label class="form-label">Category name</label>
                                    </div>
                                </div>

                                <div class="form-group">

                                    <input type="file" name="image">

                                    <img src="{{ url('storage/category/slider/'. $category->image) }}" alt="">
                                </div>
                                <br>
                                <a href="{{ route('admin.category.index') }}" class="btn btn-danger m-t-15 waves-effect">Back</a>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
</section>
@endsection

@push('js')

@endpush