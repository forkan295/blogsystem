@extends('layouts.backend.app')

@section('tittle','Post')

@push('css')
<link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css"') }}" rel="stylesheet">
@endpush
@section('contant')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">

            <a href="{{ route('admin.post.create') }}" class="btn btn-success waves-effect">Add New Post</a>
        </div>
       
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           All Posts
                           <span>{{ $data->count() }}</span>
                        </h2>
                       
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>

                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th><i class="material-icons">visibility</i></th>
                                        <th>Is Appproved</th>
                                        <th>Status</th>
                                        <th>Created at</th>
                                        <th>Uodated at</th>
                                        <th>Action</th>
                                       
                                    </tr>

                                </thead>
                                <tfoot>

                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th><i class="material-icons">visibility</i></th>
                                        <th>Is Appproved</th>
                                        <th>Status</th>
                                        <th>Created at</th>
                                        <th>Uodated at</th>
                                        <th>Action</th>
                                       
                                    </tr>

                                </tfoot>
                                <tbody>
                                       
                                   @foreach ($data as $key => $item)
                                    <tr>
                                       
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ str_limit($item->title, '10') }}</td>
                                        <td>{{ $item->user->name }}</td>
                                        <td>{{ $item->view_count  }}</td>
                                        <td>{{ $item->is_approved == true ? 'Approved' : "Pending"}}</td>
                                        <td>{{ $item->status == true ? 'Published' : "Pending"}}</td>
                                        <td>{{ $item->created_at}}</td>
                                        <td>{{ $item->updated_at}}</td> 
                                        <td>
                                            <a href="{{ route('admin.post.show',$item->id) }}" class="btn btn-primary">Show</a>
                                            <a href="{{ $item->user->role_id == 2 ? "#" : route('admin.post.edit',$item->id) }}" class="btn btn-primary" 
                                                
                                               @if ($item->user->role_id == 2)
                                                   disabled
                                               @else
                                                   
                                               @endif 
                                                
                                                
                                                >Edit</a>
                                            
                                            <button type="button"  class="btn btn-danger" onclick="deletePost({{ $item->id }})">Delete</button>
                                            <form id="delete-form-{{ $item->id }}" action="{{ route('admin.post.destroy',$item->id) }}" method="POST" style="display:none">
                                            @csrf
                                            @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                   @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
@endsection

@push('js')
<script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

<script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script type="text/javascript">
function deletePost(id)
{
    Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
    event.preventDefault();
    document.getElementById('delete-form-'+ id).submit();
  }
})

}

</script>
@endpush