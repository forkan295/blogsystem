@extends('layouts.backend.app')

@section('tittle','Post')

@push('css')
 <!-- Bootstrap Select Css -->
 <link href="{{ asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endpush
@section('contant')
<section class="content">

       <div class=>

            <a href="{{ route('admin.post.index') }}" class="btn btn-danger"><i class="material-icons">keyboard_arrow_left</i> Back</a>

           @if ($post->status == 1)
              <button disabled="disabled" class="btn btn-success"><i class="material-icons">done</i> Published </button>
           @else
            <button onclick="publishePost({{ $post->id }})" class="btn btn-dark"> <i class="material-icons">hourglass_empty</i> Pending</button>
           @endif
           
           @if ($post->is_approved == true)
              <button disabled="disabled" class="btn btn-success"><i class="material-icons">done</i> Approved </button>
             
           @else
            <button onclick="approvedPost({{ $post->id }})" class="btn btn-dark"> <i class="material-icons">hourglass_empty</i> Approve</button>
            <form action="{{ route('admin.post.approved',$post->id) }}" id="approval-form" style="display:none;" method="POST">
                @csrf
                @method('PUT')
                  </form>
           @endif

          
             
         
            
         
       </div>
       <br>

        <div class="row clearfix">
            <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>
                           {{ $post->title }}  
                           <small>Posted by <strong><a href="">{{ $post->user->name }}</a></strong> on {{ ($post->created_at) ? $post->created_at->toFormattedDateString() : 'No create date availabe' }}</small>
                        </h2>
                    
                    </div>
                    <div class="body">
                        {!! $post->body !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-blue">
                        <h2>
                           Categories name
                        </h2>
                    
                    </div>
                    <div class="body">
                       
                            @foreach ($post->categories as $category)
                                <span class="label bg-cyan">{{ $category->name }}</span>
                            @endforeach
                       
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header bg-green">
                        <h2>
                           Tags name
                        </h2>
                    
                    </div>
                    <div class="body">
                       
                            @foreach ($post->tags as $tag)
                                <span class="label bg-green">{{ $tag->name }}</span>
                            @endforeach
                       
                    </div>
                </div>
            
            
                <div class="card">
                    <div class="header bg-black">
                        <h2>
                           Featured Image
                        </h2>
                    
                    </div>
                    <div class="body">
                       
                           <img class="thumbnail" src="{{ url('storage/post/'.$post->image) }}" alt="" style="height:200px; width:340px;">
                       
                    </div>
                </div>

                
            </div>
        </div>

       
</section>
@endsection

@push('js')
 <!-- Select Plugin Js -->
 <script src="{{ asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
  <!-- Multi Select Plugin Js -->
 <script src="{{ asset('assets/backend/plugins/multi-select/js/jquery.multi-select.js') }}"></script>

 <script src="{{ asset('assets/backend/plugins/tinymce/tinymce.js') }}"></script>
     <!-- Ckeditor -->
     <script>
     
     
     $(function () {
  

    //TinyMCE
    tinymce.init({
        selector: "textarea#tinymce",
        theme: "modern",
        height: 300,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
    });
    tinymce.suffix = ".min";
    tinyMCE.baseURL = '{{ asset("assets/backend/plugins/tinymce") }}';
});
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script type="text/javascript">

function approvedPost(id)
        {
                Swal.fire({
            title: 'Are you sure?',
            text: "You wanted to approve this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Approve it!'
        }).then((result) =>
        {
            if (result.value) 
            {
                event.preventDefault();
                document.getElementById('approval-form').submit();
            }
        })

        }
    function publishePost(id)
    {
        Swal.fire({
            title: 'Are you want to Publish this post?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, i want to approve it!'
    }).then((result) => {
       
      if (result.value) {
       $(function(){
        //    alert('clicked');
         $(location).attr('href', "{{ route('admin.post.edit',$post->id)}}");
       })
        // var a = document.getElementsByClassName('route');
        // a.href = "{{ route('admin.post.edit',$post->id)}}";
      }
    })
    
    }

    
    </script>



@endpush